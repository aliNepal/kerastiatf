#Help

1. create your own environment

	``conda env --help``
	``conda create --name hdf5``

2. ``source activate hdf5``
(use: ``source deactivate`` to deactivate your environment)

3. check all environments in your machine

	``conda info --envs``

4. install all your dependencies
	
	``conda env create -f dependenciesInstall.yml``

5. creating a file with all specifical list 

	``conda list --explicit > spec-file.txt``

6. adds all these packages to the existing environment

	``conda install --name MyEnvironment --file spec-file.txt``

###Saved environment variables

1. locate the directory of the conda envs
e.g. /home/sharib/miniconda3/envs/register_ALI

	mkdir -p ./etc/conda/activate.d
	mkdir -p ./etc/conda/deactivate.d
	touch ./etc/conda/activate.d/env_vars.sh
	touch ./etc/conda/deactivate.d/env_vars.sh

2. Edit the two files

 ./etc/conda/activate.d/env_vars.sh should have this:


	#!/bin/sh

	export MY_KEY='secret-key-value'
	export MY_FILE=/path/to/my/file/

And ./etc/conda/deactivate.d/env_vars.sh should have this:

	#!/bin/sh

	unset MY_KEY
	unset MY_FILE
	

##REMOVING

 ``conda env remove -n hdf51``

####Build and contribute
	[Link]<https://conda.io/docs/building/build.html>


