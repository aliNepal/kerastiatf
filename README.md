# README

## Help:
[bitbucket](https://confluence.atlassian.com/bitbucket/copy-your-git-repository-and-add-files-746520876.html)

### Easy install 

- Guide to easy installation of keras with backend of tensorflow and theano

- A virtual environment will be set-up through 'conda'

- You do not have to wory about any dependencies or what-so-ever!

### Installation
- Pre-requisit: conda/miniconda installation

- Run:
    ``conda env create -f dependenciesInstall.yml``

- Check you package installation: set-up .bashrc or .bash_profile

		export PATH="/home/$UserName/miniconda3/bin:$PATH"
		export LD_LIBRARY_PATH="/home/$UserName/miniconda3/pkgs/hdf5-1.8.17-10/lib:$LD_LIBRARY_PATH"*

    ``source activate kerasBackEndTFlow``

### Keras

- If you have run Keras at least once, you will find the Keras configuration file at:

    ``$HOME/.keras/keras.json``

    Change: backend: string, "tensorflow" or "theano" (as per your requirement)	
 
    ``keras.backend.backend()``
    [backend](https://keras.io/backend/)


### Extra
	
- You might also be interested in image-processing using python
	
	 ``conda install scikit-image ``

- For further info: read condahelp.md and setup your virtual env. 


### Example
- Run test.py which will run train and test on MNIST_dataset
    ``$python test.py`` 
